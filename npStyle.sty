\NeedsTeXFormat{LaTeX2e}

\ProvidesPackage{npStyle}[2023/04/22 Pacchetto di stile generico per i miei file]

%% CONDIZIONALI SUL FUNZIONAMENTO DEL PACCHETTO
\RequirePackage{kvoptions}
\RequirePackage{xifthen}    % Condizionali migliori
\DeclareBoolOption[false]{images}               % Carica pacchetti per le immagini
\DeclareBoolOption[true]{approx}                % Comandi per gli ordini di approssimazione
\DeclareStringOption[english]{language}         % Selezione linguaggio
\DeclareBoolOption[true]{niceReIm}              % Nicer \Re and \Im
\DeclareBoolOption[false]{beamerCompat}         % Disabilita componenti incompatibili con beamer
\DeclareBoolOption[true]{sectionClearpage}
\DeclareBoolOption[true]{titlepage}
\DeclareBoolOption[false]{siunitx}              % Unità di misura
\ProcessKeyvalOptions*

% Convert some options to xifthen booleans
% TODO: better way of doing this
\newboolean{npStyle@scp}
\ifnpStyle@sectionClearpage
    \setboolean{npStyle@scp}{true}
\else
    \setboolean{npStyle@scp}{false}
\fi

\newboolean{npStyle@tp}
\ifnpStyle@titlepage
    \setboolean{npStyle@tp}{true}
\else
    \setboolean{npStyle@tp}{false}
\fi

%% UTILITY
\RequirePackage{xargs}      % Definizioni macro migliori
\RequirePackage{adjustbox}  % Utility per spostare cose
\RequirePackage{xspace}     % Permette di definire comandi che sembrano non mangiare spazi. Esempio: \newcommand{\Kahler}{K\"ahler\xspace}
\RequirePackage{iflang}     % Language detection conditionals

%% FONTS, ENCODING, LINGUA
\RequirePackage[T1]{fontenc}
\RequirePackage[\npStyle@language]{babel}
\RequirePackage[utf8]{inputenc}

% Assicura di avere \ap e \ped anche non in italiano
% Le definizioni sono copiate da babel-italian.dtx
\@ifundefined{ped}{
    \DeclareRobustCommand*{\ped}[1]{%
        \textormath{$_{\mbox{\fontsize\sf@size\z@
                    \selectfont#1}}$}{_\mathrm{#1}}}%
}{}
\@ifundefined{ap}{
\DeclareRobustCommand*{\ap}[1]{%
    \textormath{\textsuperscript{#1}}{^{\mathrm{#1}}}}%
}{}

\RequirePackage{xspace}
\def\Cech{\v{C}ech\xspace}

%% FORMATTAZIONE E GEOMETRIA

% ToDo: hyperref should be loaded as late as possible
\ifnpStyle@beamerCompat\else
    \RequirePackage[a4paper]{geometry}
    \RequirePackage{titlesec}
    \newcommand{\sectionbreak}{
        \ifthenelse{
            \(\boolean{npStyle@tp} \AND \value{section} = 1\)
            \OR
            \(\boolean{npStyle@scp} \AND \value{section} > 1\)
        }{\clearpage}{}
    }
\fi

% Diciamo al TeX che ci fa schifo avere righe singole di testo all'inizio o a fine pagina
\widowpenalty=10000
\clubpenalty=10000

%% HYPERLINK E BOOKMARK
\RequirePackage{hyperref}   % Genera indice e permette di cliccare nel pdf, colorlinks fa il link colorati in blu invece delle box.
\hypersetup{
%    bookmarks
    ,colorlinks
    ,linkcolor=blue
}
\usepackage{bookmark}

% IMMAGINI
\ifnpStyle@images
    \RequirePackage{graphicx}
    %    \RequirePackage{subfig}
    \RequirePackage{subcaption}
    \RequirePackage{transparent}
\fi

% PACCHETTI PER LA SCRITTURA MATEMATICA
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{mathtools}      % Simboli, ambienti, misc
\ifnpStyle@beamerCompat\else
    \RequirePackage{enumitem}   % Più controllo sugli ambienti di enumerazione
\fi
\RequirePackage{halloweenmath}  % Black magic

% Latex 3 abomination to load ecommath when using npStyle as a local path
% TODO: something less ugly
\ExplSyntaxOn
\tl_new:N \l_tmpa_cusu
\seq_put_left:NV \l_file_search_path_seq \CurrentFilePath
\RequirePackage{ecommath}
\seq_pop_left:NN \l_file_search_path_seq \l_tmpa_cusu
\ExplSyntaxOff

% MATH FONTS
\RequirePackage{mathrsfs}   % Simboli in font script
\RequirePackage{bm}         % Simboli bold migliorati, provides \bm
\RequirePackage{bbm}        % Alternative blackboard bold, use \mathbbm
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n} % Stessa cosa di bbold ma senza sostituire mathbb
\let\bb\mathbbm
\newcommand{\one}{\mathbbm{1}}      % Mathbb one
\newcommand{\zero}{\mathbbold{0}}   % Mathbb zero

% TABELLE E MATRICI (must be after amsmath include)
\RequirePackage{booktabs}    % Tabelle più belle
% Permette di specificare nelle matrici la formattazione come nelle tabelle
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
    \hskip -\arraycolsep
    \let\@ifnextchar\new@ifnextchar
    \array{#1}
}

% PHYSICS SYMBOLS AND PACKAGES
\RequirePackage{braket}               % Notazione di Dirac
\RequirePackage{tensor}               % Tensori con indici alti e bassi

\let\idx\indices
\newcommand{\Hilb}{\mathcal{H}}       % Spazio di Hilbert
\newcommand{\adj}{^\dagger}           % Aggiunto
\newcommand{\dirac}[1]{\overline{#1}} % Overline alias for Dirac conjugates
\newcommand{\PIdif}[1]{\mathcal{D}\sbr{#1}\;} % Path integral measure

% Feynman slash notation:
\RequirePackage{slashed}
\newcommand{\dslash}{\slashed{\partial}}
\newcommand{\Dslash}{\slashed{D}}
\newcommand{\pslash}{\slashed{p}}

% MATH SYMBOLS AND ALIASES

% Logic operators
\newcommand{\liff}{\Longleftrightarrow}
\renewcommand{\iff}{\Leftrightarrow}

% Number fields and rings
\newcommand{\R}{\bb{R}}
\newcommand{\C}{\bb{C}}
\newcommand{\Z}{\bb{Z}}

% Groups and algebras
\newcommand{\GL}[1]{\operatorname{GL}(#1)}
\newcommand{\SL}[1]{\operatorname{SL}(#1)}
\newcommand{\Og}[1]{\operatorname{O}(#1)}
\newcommand{\SO}[1]{\operatorname{SO}(#1)}
\newcommand{\SU}[1]{\operatorname{SU}(#1)}
\newcommand{\U}[1]{\operatorname{U}(#1)}
\newcommand{\PSL}[1]{\operatorname{PSL}(#1)}
\newcommand{\PSU}[1]{\operatorname{PSU}(#1)}
\newcommand{\Clifford}[2][]{\operatorname{C\ell}^{#1}\!\del{#2}}
\newcommand{\Spin}[1]{\operatorname{Spin}(#1)}
\newcommand{\Pin}[2][]{\operatorname{Pin}^{#1}(#2)}
\newcommand{\su}[1]{\operatorname{\mathfrak{su}}(#1)}
\newcommand{\so}[1]{\operatorname{\mathfrak{so}}(#1)}
\newcommand{\slalg}[1]{\operatorname{\mathfrak{sl}}(#1)}

% Topological spaces
\newcommand{\Sph}{\bb{S}}
\newcommand{\Trs}{\bb{T}}
\newcommand{\RP}{\bb{R}\mathrm{P}}
\newcommand{\CP}{\bb{C}\mathrm{P}}

% Algebra operators
\DeclareMathOperator{\tr}{tr}       % Trace
\DeclareMathOperator{\Ker}{Ker}     % Kernel
\DeclareMathOperator{\Imm}{Imm}     % Image
\ifnpStyle@niceReIm
    \let\Re\relax
    \let\Im\relax
    \DeclareMathOperator{\Re}{Re}   % Real part
    \DeclareMathOperator{\Im}{Im}   % Imaginary part
\fi
\DeclareMathOperator{\sgn}{sgn}     % Sign
\DeclareMathOperator{\id}{id}       % Identity
\DeclareMathOperator{\Hom}{Hom}     % Hom set
\DeclareMathOperator{\Aut}{Aut}     % Hom set
\DeclareMathOperator{\End}{End}     % Hom set

\let\arrowvec\vec
\renewcommand{\vec}[1]{{\bm{\mathbf{#1}}}}      % Vettore in grassetto corsivo
\newcommand{\vers}[1]{\widehat{\vec{#1}}}       % Versore
\newcommand{\placeholderdot}{\bullet}
%\renewcommand{\setminus}{\!\smallsetminus\!}

% ALIAS
\newcommand{\Ell}{\mathcal{L}}                  % Alias per L calligrafica
\newcommand{\x}{\vec{x}}                        % Alias per x vettore
\newcommand{\p}{\vec{p}}                        % Alias per p vettore
\newcommand{\wt}{\widetilde}                    % Alias per widetilde
\newcommand{\wh}{\widehat}                      % Alias per widehat
\newcommand{\ol}{\overline}                     % Alias per overline

% Scambio tra \epsilon e \varepsilon
\let\temp\epsilon
\let\epsilon\varepsilon
\let\varepsilon\temp

% ALTRO
\newcommand{\Domanda}{\textcolor{red}{Domanda}: }
\newcommand{\Question}{\textcolor{red}{Question}: }
\newcommand{\red}[1]{\textcolor{red}{#1}}

% ORDINI DI APPROSSIMAZIONE
\ifnpStyle@approx
    \newcommand{\ord}[1]{^{\del{#1}}}
    \newcommand{\toord}[1]{^{\sbr{#1}}}
    \newcommand{\bigo}[1]{\mathcal{O}\del{#1}}
\fi

\ifnpStyle@siunitx
    \RequirePackage{siunitx}
    \sisetup{
        exponent-product = \cdot
        ,separate-uncertainty=true
    }
\fi
